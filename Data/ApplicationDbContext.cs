﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using SugartwebApp.Models;

namespace SugartwebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
         public DbSet<Art> arts { get; set; }
         public DbSet<Artist> artists { get; set; }
        public DbSet<Emailcs> emails { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ArtistArt>()
                .HasKey(t => new { t.artId, t.artistId });

            builder.Entity<ArtistArt>()
                .HasOne(ri => ri.art).WithMany(i => i.ArtistArts)
                .HasForeignKey(ri => ri.artId);

            builder.Entity<ArtistArt>()
                .HasOne(ri => ri.artist)
                .WithMany(i => i.ArtistArts)
                .HasForeignKey(ri => ri.artistId);
        }

    }
}
