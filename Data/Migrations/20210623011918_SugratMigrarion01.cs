﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SugartwebApp.Data.Migrations
{
    public partial class SugratMigrarion01 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "artists",
                columns: table => new
                {
                    artistId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    artistName = table.Column<string>(nullable: true),
                    artistCountry = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_artists", x => x.artistId);
                });

            migrationBuilder.CreateTable(
                name: "arts",
                columns: table => new
                {
                    artId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    artTitle = table.Column<string>(nullable: true),
                    artValue = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_arts", x => x.artId);
                });

            migrationBuilder.CreateTable(
                name: "ArtistArt",
                columns: table => new
                {
                    artId = table.Column<int>(nullable: false),
                    artistId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ArtistArt", x => new { x.artId, x.artistId });
                    table.ForeignKey(
                        name: "FK_ArtistArt_arts_artId",
                        column: x => x.artId,
                        principalTable: "arts",
                        principalColumn: "artId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ArtistArt_artists_artistId",
                        column: x => x.artistId,
                        principalTable: "artists",
                        principalColumn: "artistId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ArtistArt_artistId",
                table: "ArtistArt",
                column: "artistId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ArtistArt");

            migrationBuilder.DropTable(
                name: "arts");

            migrationBuilder.DropTable(
                name: "artists");
        }
    }
}
