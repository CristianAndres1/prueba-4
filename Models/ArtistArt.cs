﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SugartwebApp.Models
{
    public class ArtistArt
    {
        public int artId { get; set; }
        public virtual Art art { get; set; }
        public int artistId { get; set; }
        public Artist artist { get; set; }

    }
}
