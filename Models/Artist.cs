﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SugartwebApp.Models
{
    public class Artist
    {
        public int artistId { get; set; }
        public string artistName { get; set; }
        public string artistCountry { get; set; }

        public ICollection<ArtistArt> ArtistArts { get; set; }
    }
}
