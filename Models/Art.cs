﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SugartwebApp.Models
{
    public class Art
    {
        public int artId { get; set; }
        public string artTitle { get; set; }
        public double artValue { get; set; }

        public ICollection<ArtistArt> ArtistArts { get; set; }

    }
}
