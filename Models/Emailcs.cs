﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SugartwebApp.Models
{
    public class Emailcs
    {
        [Key]
        public int emailId { get; set; }
        public string emailFrom { get; set; }
        public string emailSubject { get; set; }
        public string emailBody { get; set; }
        public string emailTo { get; set; }
    }
}
