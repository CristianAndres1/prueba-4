﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SugartwebApp.Data;
using SugartwebApp.Models;

namespace SugartwebApp.Controllers
{
    public class ArtsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ArtsController(ApplicationDbContext context)
        {
            _context = context;
        }

        //Vista de Listado publico 
        public async Task<IActionResult> BuscarObra()
        {
            return View();
        }

        public async Task<IActionResult> IndexPublic2()
        {
            return View(await _context.arts.ToListAsync());
        }
        //buscador
        public async Task<IActionResult> ResultadoDeBusqueda(String PalabraBuscada) //Se pasa como parametro la palabra buscada
        {
            //String ResultadoDeBusqueda
            //  return "Buscaste la palabra "+ PalabraBuscada; (Se pasa al metodo a string para comprobar el funcionamiento)
            return View("IndexPublic", await _context.arts.Where(b => b.artTitle.Contains(PalabraBuscada)).ToListAsync());
            //Funcion flecha que retorna el index filtrado por la palabra que se ingreso 
        }

        [Authorize]
        // GET: Arts
        public async Task<IActionResult> Index()
        {
            return View(await _context.arts.ToListAsync());
        }


        [Authorize]
        // GET: Arts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var art = await _context.arts
                .FirstOrDefaultAsync(m => m.artId == id);
            if (art == null)
            {
                return NotFound();
            }

            return View(art);
        }

        [Authorize]
        // GET: Arts/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Arts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("artId,artTitle,artValue")] Art art)
        {
            if (ModelState.IsValid)
            {
                _context.Add(art);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(art);
        }

        [Authorize]
        // GET: Arts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var art = await _context.arts.FindAsync(id);
            if (art == null)
            {
                return NotFound();
            }
            return View(art);
        }

        // POST: Arts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Edit(int id, [Bind("artId,artTitle,artValue")] Art art)
        {
            if (id != art.artId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(art);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ArtExists(art.artId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(art);
        }

        [Authorize]
        // GET: Arts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var art = await _context.arts
                .FirstOrDefaultAsync(m => m.artId == id);
            if (art == null)
            {
                return NotFound();
            }

            return View(art);
        }

        [Authorize]
        // POST: Arts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var art = await _context.arts.FindAsync(id);
            _context.arts.Remove(art);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ArtExists(int id)
        {
            return _context.arts.Any(e => e.artId == id);
        }
    }
}
