﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SugartwebApp.Data;
using SugartwebApp.Models;

using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace SugartwebApp.Controllers
{
    public class EmailcsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EmailcsController(ApplicationDbContext context)
        {
            _context = context;
        }

        [Authorize]
        // GET: Emailcs
        public async Task<IActionResult> Index()
        {
            return View(await _context.emails.ToListAsync());
        }

        [Authorize]
        // GET: Emailcs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emailcs = await _context.emails
                .FirstOrDefaultAsync(m => m.emailId == id);
            if (emailcs == null)
            {
                return NotFound();
            }

            return View(emailcs);
        }

        // GET: Emailcs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Emailcs/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("emailId,emailFrom,emailSubject,emailBody,emailTo")] Emailcs emailcs)
        {
            //Metodo para envio de mails
            MailMessage correo = new MailMessage();

            correo.From = new MailAddress(emailcs.emailFrom, "Contacto Sugart®", System.Text.Encoding.UTF8);//email de origen
            correo.To.Add("cristian.andres.dev.send.mail@gmail.com");//email que recibe
            correo.Subject = emailcs.emailSubject; //Asunto 
            correo.Body = emailcs.emailBody;//cuerpo del correo
            correo.IsBodyHtml = true;
            correo.Priority = MailPriority.Normal;

            SmtpClient smtp = new SmtpClient();
            smtp.UseDefaultCredentials = false;
            smtp.Host = "smtp.gmail.com";//servicio smtp de gmail
            smtp.Port = 587;//puerto
            smtp.Credentials = new System.Net.NetworkCredential("cristian.andres.dev.send.mail@gmail.com", "Clave-10");//credenciales gmail usuario
            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
            smtp.EnableSsl = true;

            smtp.Send(correo);
            smtp.Dispose();


            if (ModelState.IsValid)
            {
                _context.Add(emailcs);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(HomeController.Index));
            }
            return RedirectToAction(nameof(HomeController.Index));
            
        }

        [Authorize]
        // GET: Emailcs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emailcs = await _context.emails.FindAsync(id);
            if (emailcs == null)
            {
                return NotFound();
            }
            return View(emailcs);
        }

        [Authorize]
        // POST: Emailcs/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("emailId,emailFrom,emailSubject,emailBody,emailTo")] Emailcs emailcs)
        {
            if (id != emailcs.emailId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(emailcs);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmailcsExists(emailcs.emailId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(emailcs);
        }

        [Authorize]
        // GET: Emailcs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var emailcs = await _context.emails
                .FirstOrDefaultAsync(m => m.emailId == id);
            if (emailcs == null)
            {
                return NotFound();
            }

            return View(emailcs);
        }

        [Authorize]
        // POST: Emailcs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var emailcs = await _context.emails.FindAsync(id);
            _context.emails.Remove(emailcs);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EmailcsExists(int id)
        {
            return _context.emails.Any(e => e.emailId == id);
        }
    }
}
